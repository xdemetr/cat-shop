import React, {Component} from 'react';
import './product-details.css';
import './__value';

export default class ProductDetails extends Component {
  declination(quantity, titles, label='') {
    quantity = parseInt(quantity, 0);
    var cases = [2, 0, 1, 1, 1, 5];
    if (quantity !== 1) {
      return [
          titles[
              (quantity % 100 > 4 && quantity % 100 < 20) ? 2 : cases[(quantity % 10 < 5) ? quantity % 10 : 5]
              ], " "+label];
    } else {
      return `${titles[0]} ${label}`;
    }
  };
  
  itemTemplate (value, name) {
    const itemValue = value > 1 ? <span className="product-details__value">{value}</span> : null;
    const itemName = name ? <span className="product-details__name">{name}</span> : null;
    
    return (
        <div className="product-details__item">
          {itemValue}
          {itemName}
        </div>
    )
  };
  
  render(){
    const {mix, packages, gift} = this.props;
    
    const count = this.itemTemplate(packages, this.declination(packages, ['порция','порции','порций']));
    const giftCount = this.itemTemplate(gift, this.declination(gift, ['мышь', 'мыши', 'мышей'],'в подарок'));
    const additional = packages > 50 ? this.itemTemplate(null, 'Заказчик доволен'): null;
    
    return (
        <ul className={[mix, 'product-details'].join(' ')}>
          {count}
          {giftCount}
          {additional}
        </ul>
    )
  };
};
