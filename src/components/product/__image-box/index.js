import React from 'react';
import './product__image-box.css';
import 'components/product/__image';

class ProductImageBox extends React.Component {
  render(){
    var title = this.props.title;
    var img = this.props.img;
    
    return(
        <div className="product__image-box">
          <img className="product__image" src={img} alt={title} />
        </div>
    )
  }
}

export default ProductImageBox;
