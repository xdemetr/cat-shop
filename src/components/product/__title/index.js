import React from 'react';
import './product__title.css';
import 'components/product/__subtitle';

const ProductTitle = ({title, subtitle}) => {
  return (
      <h2 className="product__title">
        {title}
        <span className="product__subtitle">{subtitle}</span>
      </h2>
  )
};

export default ProductTitle;
