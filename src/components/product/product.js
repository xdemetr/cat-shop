import React, {Component} from 'react';
import './__content';
import './__weight';
import './__card';
import './__offer';
import './__details';
import './__aside';
import './__description';
import './__notice';
import './_selected';
import './_disabled';
import 'components/pseudo-link';

import ProductTitle from './__title';
import ProductImageBox from './__image-box';
import ProductDetails from 'components/product-details';
import ProductWeight from 'components/product-weight';

export default class Product extends Component {
  state = {
    hoverSelected: false
  };
  
  onHover(selected){
    if(selected) {
      this.setState({
        hoverSelected: true
      });
    }
  };
  
  onHoverOut(){
    this.setState({
      hoverSelected: false
    });
  };
  
  render() {
    const {
      title, subTitle, description, weight, packages, gift,
      disabled, selected,
      onToggleSelected
    } = this.props;
    
    const {hoverSelected} = this.state;
    
    let itemClass = 'product';
    let asideContent = <Action onToggleSelected={onToggleSelected} />;
    
    if (selected) {
      itemClass = `${itemClass} product_selected`;
      asideContent = description;
    }
    
    if (disabled) {
      itemClass = `${itemClass} product_disabled`;
      asideContent = `Печалька, ${subTitle} закончился.`;
    }
    
    const offerContent = hoverSelected ? <Notice /> : <Teaser />;
    
    return (
        <section className={itemClass}>
          <div className="product__card"
               onClick={onToggleSelected}
               onMouseOver={ () => this.onHover(selected) }
               onMouseOut={ () => this.onHoverOut() }
          >
            
            <ProductOffer offer={offerContent}/>
            
            <div className="product__content">
              <ProductTitle title={title} subtitle={subTitle} />
              <ProductDetails packages={packages} gift={gift} mix="product__details" />
              <ProductWeight value={weight} mix="product__weight" />
              <ProductImageBox title={title} img={require('./product.png')} />
            </div>
          </div>
          
          <div className="product__aside">
            <div className="product__description">{asideContent}</div>
          </div>
        </section>
    )
  }
};

const ProductOffer = ({offer}) => {
  return(
      <React.Fragment>
        <div className="product__offer">
          {offer}
        </div>
      </React.Fragment>
  )
};

const Teaser = () => {
  return(
      <React.Fragment>
        <p className="product__teaser">Сказочное заморское явство</p>
      </React.Fragment>
  )
};

const Notice = () => {
  return(
      <React.Fragment>
        <p className="product__notice">Котэ не одобряет?</p>
      </React.Fragment>
  )
};

const Action = ({onToggleSelected}) => {
  return(
      <React.Fragment>
        Чего сидишь? Порадуй котэ, <span className="pseudo-link" onClick={onToggleSelected}>купи</span>.
      </React.Fragment>
  )
}
