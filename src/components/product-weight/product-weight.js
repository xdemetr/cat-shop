import React from 'react';

import './product-weight.css';
import './__label';
import './__between-text';
import './__value';
import './__unit';

const ProductWeight = ({mix, value}) => {
  return(
      <div className={[mix, 'product-weight',].join(' ')}>
        <span className="product-weight__label">Вес:</span>
        <span className="product-weight__between-text">&nbsp;</span>
        <span className="product-weight__value">{value}</span>
        <span className="product-weight__between-text">&nbsp;</span>
        <span className="product-weight__unit">кг.</span>
      </div>
  )
};

export default ProductWeight;
