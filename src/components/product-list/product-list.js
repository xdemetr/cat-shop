import React from 'react';

import './product-list.css';
import './__item';
import Product from 'components/product';

const ProductList = ({products, onToggleSelected}) => {
  const elements = products.map((item) => {
    const {id, ...itemProps} = item;
    return (
        <li className="product-list__item" key={id}>
          <Product
              {...itemProps}
              onToggleSelected={ ()=>onToggleSelected(id) }
          />
        </li>
    )
  });
  
  return (
      <div className="product-list">
        {elements}
      </div>
  )
};

export default ProductList;
