import React, {Component} from 'react';

import './app.css';
import './__layout';
import './__header';
import './__title';
import 'components/font/__exo2';

import ProductList from 'components/product-list';

export default class App extends Component {
  
  startId = 1;
  
  state = {
    title: "Ты сегодня покормил кота?",
    products : [
      this.createProduct('Нямушка', 'с фуа-гра', 'Печень утки разварная с артишоками.', '0,5', 10, 1),
      this.createProduct('Нямушка', 'с рыбой', 'Головы щучьи да свежайшая сёмгушка.', '2', 40, 2),
      this.createProduct('Нямушка', 'с курой', 'Филе из цыплят с трюфелями в бульоне.', '5', 100, 5, true)
    ]
  };
  
  createProduct (title, subTitle, description, weight, packages, gift, disabled = false, selected = false ) {
    return {
      id: this.startId++,
      title: title,
      subTitle: subTitle,
      description: description,
      weight: weight,
      packages: packages,
      gift: gift,
      disabled: disabled,
      selected: selected,
    }
  }
  
  onToggleSelected = (id) => {
    this.setState (({products}) => {
      const idx = products.findIndex((el)=> el.id === id);
      const oldItem = products[idx];
      const newItem = {...oldItem, selected: !oldItem.selected};
      const newArray = [
        ...products.slice(0, idx),
        newItem,
        ...products.slice(idx+1)
      ];
      return {
        products: newArray
      }
    });
  };
  
  render() {
    const {title, products} = this.state;
    
    return (
        <div className="app">
          <AppHeader title={title} />
          <AppBody products={products} onToggleSelected={this.onToggleSelected} />
        </div>
    )
  }
}

const AppHeader = ({title}) => {
  return(
      <React.Fragment>
        <div className="app__header">
          <div className="app__layout">
            <h1 className="app__title">{title}</h1>
          </div>
        </div>
      </React.Fragment>
  )
};

const AppBody = ({products, onToggleSelected})=> {
  return(
      <React.Fragment>
        <div className="app__body">
          <div className="app__layout">
            <ProductList
                products={products}
                onToggleSelected={onToggleSelected}
            />
          </div>
        </div>
      </React.Fragment>
  )
};
